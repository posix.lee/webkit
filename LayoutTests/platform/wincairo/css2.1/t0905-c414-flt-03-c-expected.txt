layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x292
  RenderBlock {HTML} at (0,0) size 800x292
    RenderBody {BODY} at (8,16) size 784x40
      RenderBlock {P} at (0,0) size 784x40 [color=#000080]
        RenderText {#text} at (0,0) size 755x39
          text run at (0,0) width 389: "The blue boxes below should be in numeric order and form a "
          text run at (389,0) width 366: "backwards \"D\". (Boxes labelled \"A\" should be in the first"
          text run at (0,20) width 55: "column; "
          text run at (55,20) width 402: "boxes labelled \"B\" should be in the second; arrows indicate the "
          text run at (457,20) width 118: "correct alignment.)"
      RenderBlock {DIV} at (16,56) size 320x0
        RenderBlock (floating) {P} at (190,4) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (77,9) size 42x19
            text run at (77,9) width 42: "B 2 \x{21E8}"
        RenderBlock (floating) {P} at (56,4) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (77,9) size 42x19
            text run at (77,9) width 42: "A 1 \x{21E8}"
        RenderBlock (floating) {P} at (190,48) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (77,9) size 42x19
            text run at (77,9) width 42: "B 4 \x{21E8}"
        RenderBlock (floating) {P} at (4,48) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (7,9) size 42x19
            text run at (7,9) width 42: "\x{21E6} A 3"
        RenderBlock (floating) {P} at (190,92) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (77,9) size 42x19
            text run at (77,9) width 42: "B 6 \x{21E8}"
        RenderBlock (floating) {P} at (4,92) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (7,9) size 42x19
            text run at (7,9) width 42: "\x{21E6} A 5"
        RenderBlock (floating) {P} at (4,136) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (7,9) size 42x19
            text run at (7,9) width 42: "\x{21E6} A 7"
        RenderBlock (floating) {P} at (190,136) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (77,9) size 42x19
            text run at (77,9) width 42: "B 8 \x{21E8}"
        RenderBlock (floating) {P} at (190,180) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (69,9) size 50x19
            text run at (69,9) width 50: "B 10 \x{21E8}"
        RenderBlock (floating) {P} at (56,180) size 126x36 [color=#FFFFFF] [bgcolor=#000080] [border: (3px solid #0000FF)]
          RenderText {#text} at (77,9) size 42x19
            text run at (77,9) width 42: "A 9 \x{21E8}"
