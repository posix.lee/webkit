layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x209
  RenderBlock {HTML} at (0,0) size 800x209
    RenderBody {BODY} at (8,16) size 784x177
      RenderBlock {P} at (0,0) size 784x20 [color=#000080]
        RenderText {#text} at (0,0) size 262x19
          text run at (0,0) width 262: "The word \"fail\" should not appear below."
      RenderBlock {DIV} at (16,36) size 240x141 [color=#FFFFFF] [bgcolor=#FFFFFF]
        RenderBlock (floating) {P} at (0,0) size 43x21 [color=#000080]
          RenderText {#text} at (0,0) size 43x20
            text run at (0,0) width 43: "TEST:"
        RenderBlock (floating) {P} at (0,20) size 240x21 [color=#00FFFF] [bgcolor=#008080]
          RenderText {#text} at (101,0) size 38x20
            text run at (101,0) width 38: "PASS"
        RenderText {#text} at (42,0) size 236x140
          text run at (42,0) width 189: "fail fail fail fail fail fail fail fail"
          text run at (0,40) width 48: "fail fail "
          text run at (48,40) width 188: "fail fail fail fail fail fail fail fail"
          text run at (0,60) width 48: "fail fail "
          text run at (48,60) width 188: "fail fail fail fail fail fail fail fail"
          text run at (0,80) width 48: "fail fail "
          text run at (48,80) width 188: "fail fail fail fail fail fail fail fail"
          text run at (0,100) width 48: "fail fail "
          text run at (48,100) width 188: "fail fail fail fail fail fail fail fail"
          text run at (0,120) width 44: "fail fail"
