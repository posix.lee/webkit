layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x352
  RenderBlock {HTML} at (0,0) size 800x352
    RenderBody {BODY} at (8,8) size 784x336
      RenderBlock {DIV} at (0,0) size 784x20
        RenderText {#text} at (0,0) size 341x19
          text run at (0,0) width 341: "The blue bits of text should be decorated as described."
      RenderBlock {P} at (0,36) size 784x264 [color=#C0C0C0]
        RenderText {#text} at (0,0) size 759x61
          text run at (0,0) width 474: "dummy text dummy text dummy text dummy text dummy text dummy text "
          text run at (474,0) width 285: "dummy text dummy text dummy text dummy"
          text run at (0,20) width 185: "text dummy text dummy text "
          text run at (185,20) width 474: "dummy text dummy text dummy text dummy text dummy text dummy text "
          text run at (659,20) width 75: "dummy text"
          text run at (0,42) width 395: "dummy text dummy text dummy text dummy text dummy text "
        RenderInline {SPAN} at (0,0) size 759x59 [color=#0000FF] [border: none (10px double #0000FF)]
          RenderText {#text} at (405,42) size 174x19
            text run at (405,42) width 174: "\x{21E6} two blue lines to the left "
          RenderInline {SPAN} at (0,0) size 759x59 [color=#C0C0C0]
            RenderText {#text} at (579,42) size 759x59
              text run at (579,42) width 52: "dummy "
              text run at (631,42) width 102: "text dummy text"
              text run at (0,62) width 368: "dummy text dummy text dummy text dummy text dummy "
              text run at (368,62) width 391: "text dummy text dummy text dummy text dummy text dummy"
              text run at (0,82) width 23: "text"
          RenderText {#text} at (23,82) size 4x19
            text run at (23,82) width 4: " "
        RenderText {#text} at (27,82) size 759x101
          text run at (27,82) width 395: "dummy text dummy text dummy text dummy text dummy text "
          text run at (422,82) width 312: "dummy text dummy text dummy text dummy text"
          text run at (0,102) width 158: "dummy text dummy text "
          text run at (158,102) width 474: "dummy text dummy text dummy text dummy text dummy text dummy text "
          text run at (632,102) width 127: "dummy text dummy"
          text run at (0,122) width 343: "text dummy text dummy text dummy text dummy text "
          text run at (343,122) width 391: "dummy text dummy text dummy text dummy text dummy text"
          text run at (0,142) width 79: "dummy text "
          text run at (79,142) width 474: "dummy text dummy text dummy text dummy text dummy text dummy text "
          text run at (553,142) width 206: "dummy text dummy text dummy"
          text run at (0,164) width 185: "text dummy text dummy text "
        RenderInline {SPAN} at (0,0) size 196x19 [color=#0000FF] [border: none (1px solid #0000FF)]
          RenderText {#text} at (186,164) size 195x19
            text run at (186,164) width 195: "\x{21E6} one thin blue line to the left "
        RenderText {#text} at (381,164) size 772x99
          text run at (381,164) width 52: "dummy "
          text run at (433,164) width 339: "text dummy text dummy text dummy text dummy text"
          text run at (0,184) width 131: "dummy text dummy "
          text run at (131,184) width 474: "text dummy text dummy text dummy text dummy text dummy text dummy "
          text run at (605,184) width 154: "text dummy text dummy"
          text run at (0,204) width 316: "text dummy text dummy text dummy text dummy "
          text run at (316,204) width 418: "text dummy text dummy text dummy text dummy text dummy text"
          text run at (0,224) width 52: "dummy "
          text run at (52,224) width 474: "text dummy text dummy text dummy text dummy text dummy text dummy "
          text run at (526,224) width 233: "text dummy text dummy text dummy"
          text run at (0,244) width 237: "text dummy text dummy text dummy "
          text run at (237,244) width 181: "text dummy text dummy text"
      RenderBlock {DIV} at (0,316) size 784x20
        RenderText {#text} at (0,0) size 416x19
          text run at (0,0) width 416: "(All the lines of text in the block above should be equally spaced.)"
