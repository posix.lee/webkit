layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x40
        RenderText {#text} at (0,0) size 776x39
          text run at (0,0) width 776: "This test ensures selection that crosses the shadow DOM boundary of a readonly input element cannot be made by a mouse"
          text run at (0,20) width 32: "drag."
      RenderBlock {P} at (0,56) size 784x40
        RenderText {#text} at (0,0) size 775x39
          text run at (0,0) width 775: "To manually test, select text by a mouse drag starting in \"world\" and ending in \"hello\". Selection should not extend into the"
          text run at (0,20) width 89: "input element."
      RenderBlock {DIV} at (0,112) size 784x26
        RenderTextControl {INPUT} at (0,0) size 63x26 [bgcolor=#FFFFFF] [border: (2px inset #808080)]
        RenderText {#text} at (63,4) size 4x19
          text run at (63,4) width 4: " "
        RenderInline {SPAN} at (0,0) size 37x19
          RenderText {#text} at (67,4) size 37x19
            text run at (67,4) width 37: "world"
        RenderText {#text} at (0,0) size 0x0
layer at (11,123) size 57x20
  RenderBlock {DIV} at (3,3) size 57x20
    RenderText {#text} at (0,0) size 35x20
      text run at (0,0) width 35: "hello"
selection start: position 0 of child 2 {#text} of child 5 {DIV} of body
selection end:   position 2 of child 0 {#text} of child 3 {SPAN} of child 5 {DIV} of body
