EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 3 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 0 of DIV > DIV > DIV > BODY > HTML > #document to 0 of DIV > DIV > DIV > BODY > HTML > #document toDOMRange:range from 0 of DIV > DIV > DIV > BODY > HTML > #document to 0 of DIV > DIV > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {DIV} at (0,0) size 784x378 [border: (2px solid #0000FF)]
        RenderBlock {DIV} at (14,14) size 756x111
          RenderText {#text} at (0,0) size 59x26
            text run at (0,0) width 59: "Tests:"
          RenderBR {BR} at (59,0) size 0x26
          RenderText {#text} at (0,27) size 611x27
            text run at (0,27) width 611: "A scenario I thought of based on my experiences with this bug:"
          RenderInline {A} at (0,0) size 260x27 [color=#0000EE]
            RenderText {#text} at (0,55) size 260x27
              text run at (0,55) width 260: "<rdar://problem/4045521>"
          RenderText {#text} at (260,55) size 744x55
            text run at (260,55) width 484: " Hitting return key with full line selected does not"
            text run at (0,83) width 254: "add blank line as it should"
        RenderBlock {DIV} at (14,141) size 756x223
          RenderBlock (anonymous) at (0,0) size 756x111
            RenderText {#text} at (0,0) size 183x26
              text run at (0,0) width 183: "Expected Results:"
            RenderBR {BR} at (183,0) size 0x26
            RenderText {#text} at (0,27) size 748x83
              text run at (0,27) width 724: "Should see this content in the red box below (note that there should be two"
              text run at (0,55) width 748: "blank lines between \"foo\" and \"baz\"; also note that the insertion point should"
              text run at (0,83) width 429: "be at the start of the third line, a blank line):"
          RenderBlock {DIV} at (0,111) size 756x28
            RenderText {#text} at (0,0) size 32x27
              text run at (0,0) width 32: "foo"
          RenderBlock {DIV} at (0,139) size 756x28
            RenderBR {BR} at (0,0) size 0x27
          RenderBlock {DIV} at (0,167) size 756x28
            RenderBR {BR} at (0,0) size 0x27
          RenderBlock {DIV} at (0,195) size 756x28
            RenderText {#text} at (0,0) size 34x27
              text run at (0,0) width 34: "baz"
      RenderBlock {DIV} at (0,402) size 784x116
        RenderBlock {DIV} at (0,0) size 784x116 [border: (2px solid #FF0000)]
          RenderBlock {DIV} at (2,2) size 780x28
            RenderText {#text} at (0,0) size 32x27
              text run at (0,0) width 32: "foo"
          RenderBlock {DIV} at (2,30) size 780x28
            RenderBR {BR} at (0,0) size 0x27
          RenderBlock {DIV} at (2,58) size 780x28
            RenderBR {BR} at (0,0) size 0x27
          RenderBlock {DIV} at (2,86) size 780x28
            RenderText {#text} at (0,0) size 34x27
              text run at (0,0) width 34: "baz"
caret: position 0 of child 0 {BR} of child 3 {DIV} of child 1 {DIV} of child 3 {DIV} of body
