layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x40
        RenderText {#text} at (0,0) size 52x19
          text run at (0,0) width 52: "Test for "
        RenderInline {I} at (0,0) size 768x39
          RenderInline {A} at (0,0) size 307x19 [color=#0000EE]
            RenderText {#text} at (52,0) size 307x19
              text run at (52,0) width 307: "https://bugs.webkit.org/show_bug.cgi?id=18818"
          RenderText {#text} at (359,0) size 768x39
            text run at (359,0) width 4: " "
            text run at (363,0) width 405: "REGRESSION (3.1.1-TOT): Character order (float:left ordered"
            text run at (0,20) width 121: "after the first letter)"
        RenderText {#text} at (121,20) size 4x19
          text run at (121,20) width 4: "."
      RenderBlock {P} at (0,56) size 784x20
        RenderText {#text} at (0,0) size 200x19
          text run at (0,0) width 200: "The next line should say \x{201C}123\x{201D}."
      RenderBlock {DIV} at (0,92) size 784x20
        RenderBlock (floating) {SPAN} at (0,0) size 8x20
          RenderText {#text} at (0,0) size 8x19
            text run at (0,0) width 8: "1"
        RenderBlock (floating) at (8,0) size 8x20
          RenderText at (0,0) size 8x19
            text run at (0,0) width 8: "2"
        RenderText {#text} at (16,0) size 8x19
          text run at (16,0) width 8: "3"
