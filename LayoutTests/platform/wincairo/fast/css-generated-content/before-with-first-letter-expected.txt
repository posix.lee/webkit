layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x40
        RenderText {#text} at (0,0) size 52x19
          text run at (0,0) width 52: "Test for "
        RenderInline {I} at (0,0) size 655x39
          RenderInline {A} at (0,0) size 301x19 [color=#0000EE]
            RenderText {#text} at (52,0) size 301x19
              text run at (52,0) width 301: "http://bugs.webkit.org/show_bug.cgi?id=14221"
          RenderText {#text} at (353,0) size 655x39
            text run at (353,0) width 4: " "
            text run at (357,0) width 298: "Repro crash (ASSERTION FAILED: oldText in"
            text run at (0,20) width 562: "RenderBlock::updateFirstLetter() during relayout of :before content with first-letter style)"
        RenderText {#text} at (562,20) size 4x19
          text run at (562,20) width 4: "."
      RenderBlock {DIV} at (0,56) size 784x34
        RenderInline (generated) at (0,0) size 47x27
          RenderInline (generated) at (0,0) size 18x33 [color=#008000]
            RenderText at (0,0) size 18x33
              text run at (0,0) width 18: "T"
          RenderText at (18,5) size 29x27
            text run at (18,5) width 29: "he "
        RenderText {#text} at (47,5) size 413x27
          text run at (47,5) width 413: "first letter is green and larger than the rest."
